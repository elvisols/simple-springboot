							
							Simple Spring Service

/************************************************************************/

* Start application on port 8080 
  
NOTE: Before running the Unit Test, kindly comment out the codes contained in
	
	@com.sols.serverSpringBootApplication;
    public void run(String... arg0) throws Exception {
		// ...
	}

For authentication/authorization, use the code below:

INITIAL REQUEST for token (from authorization server):
URL: http://localhost:8080/oauth/token
METHOD: POST
Headers:
  - content-type : application/x-www-form-urlencoded; charset=utf-8
  - authorization : Basic Zm9vQ2xpZW50SWRQYXNzd29yZDpzZWNyZXQ=   
Body: 
  - username: elvis
  - password: 123
  - grant_type: password
  - client_id: fooClientIdPassword

Sample Response:
{
	"access_token": "7261cefb-c170-42d5-951a-46963211cac7",
	"token_type": "bearer",
	"refresh_token": "70c46318-bb8d-4398-8d6e-1f351f0a3df9",
	"expires_in": 3599,
	"scope": "foo read write",
	"client": "elviszmxk"
}

SUBSEQUENT REQUESTS (to secured resource endpoints [v1/cars and v1/drivers]):
Headers:
  - content-type : application/json
  - authorization : Bearer 7261cefb-c170-42d5-951a-46963211cac7 # Gotten from above



/*************  Thank you ! ************/  
  