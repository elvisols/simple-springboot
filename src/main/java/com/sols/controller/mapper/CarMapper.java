package com.sols.controller.mapper;

import com.sols.datatransferobject.CarDTO;
import com.sols.domainobject.CarDO;
import com.sols.domainobject.DriverDO;
import com.sols.domainvalue.EngineType;
import com.sols.domainvalue.GeoCoordinate;
import com.sols.domainvalue.Manufacturer;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

public class CarMapper
{
    public static CarDO makeCarDO(CarDTO carDTO)
    {
        return new CarDO(carDTO.getLicense_plate(), carDTO.getSeat_count(), carDTO.getRating(), carDTO.getConvertible(), new Manufacturer(carDTO.getManufacturer()), EngineType.valueOf(carDTO.getEngineType()));
    }


    public static CarDTO makeCarDTO(CarDO carDO)
    {
        CarDTO.CarDTOBuilder carDTOBuilder = CarDTO.newBuilder()
            .setId(carDO.getId())
            .setLicense_plate(carDO.getLicense_plate())
            .setSeat_count(carDO.getSeat_count())
            .setConvertible(carDO.getConvertible())
            .setEngineType(carDO.getEngineType().toString())
            .setRating(carDO.getRating());

        Manufacturer manufacturer = carDO.getManufacturer();
        if (manufacturer != null)
        {
            carDTOBuilder.setManufacturer(manufacturer.getName());
        }
        
        DriverDO driverDO = carDO.getDriverDO();
        if (driverDO != null) {
            carDTOBuilder.setDriverDO(driverDO);
        }
        
        return carDTOBuilder.createCarDTO();
    }


    public static List<CarDTO> makeCarDTOList(Collection<CarDO> cars)
    {
        return cars.stream()
        	.filter(x -> x.getId() != 0)
            .map(CarMapper::makeCarDTO)
            .collect(Collectors.toList());
    }
}
