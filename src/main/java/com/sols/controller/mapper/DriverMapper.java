package com.sols.controller.mapper;

import com.sols.datatransferobject.DriverDTO;
import com.sols.domainobject.CarDO;
import com.sols.domainobject.DriverDO;
import com.sols.domainvalue.GeoCoordinate;
import com.sols.exception.EntityNotFoundException;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

public class DriverMapper
{
    public static DriverDO makeDriverDO(DriverDTO driverDTO)
    {
        return new DriverDO(driverDTO.getUsername(), driverDTO.getPassword());
    }


    public static DriverDTO makeDriverDTO(DriverDO driverDO)
    {
        DriverDTO.DriverDTOBuilder driverDTOBuilder = DriverDTO.newBuilder()
            .setId(driverDO.getId())
            .setPassword(driverDO.getPassword())
            .setUsername(driverDO.getUsername());
        
        GeoCoordinate coordinate = driverDO.getCoordinate();
        if (coordinate != null)
        {
            driverDTOBuilder.setCoordinate(coordinate);
        }
        
        CarDO carDO = driverDO.getCarDo();
        if(carDO != null) {
        	driverDTOBuilder.setCarDO(carDO);
        }

        return driverDTOBuilder.createDriverDTO();
    }
    
    public static DriverDTO makeCarDTO(DriverDO driverDO) throws EntityNotFoundException
    {
    	DriverDTO.DriverDTOBuilder driverDTOBuilder = DriverDTO.newBuilder();

    	CarDO carDO = driverDO.getCarDo();
        if(carDO != null) {
        	driverDTOBuilder.setCarDO(carDO);
        } else {
        	throw new EntityNotFoundException("Could not find car for driver with id: " + driverDO.getId());
        }
    	
    	return driverDTOBuilder.createDriverDTO();
    }


    public static List<DriverDTO> makeDriverDTOList(Collection<DriverDO> drivers)
    {
        return drivers.stream()
            .map(DriverMapper::makeDriverDTO)
            .collect(Collectors.toList());
    }
    
    public static List<DriverDTO> makeDriverDTOList2(Collection<DriverDO> drivers)
    {
    	return drivers.stream()
    			.filter(x -> x.getCarDo().getRating() > 3)
    			.map(DriverMapper::makeDriverDTO)
    			.collect(Collectors.toList());
    }
}
