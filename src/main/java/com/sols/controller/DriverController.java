package com.sols.controller;

import com.sols.controller.mapper.DriverMapper;
import com.sols.datatransferobject.DriverDTO;
import com.sols.domainobject.DriverDO;
import com.sols.domainvalue.EngineType;
import com.sols.domainvalue.OnlineStatus;
import com.sols.exception.CarAlreadyInUseException;
import com.sols.exception.ConstraintsViolationException;
import com.sols.exception.EntityNotFoundException;
import com.sols.service.car.CarService;
import com.sols.service.driver.DriverService;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

/**
 * All operations with a driver will be routed by this controller.
 * <p/>
 */
@RestController
@RequestMapping("v1/drivers")
public class DriverController
{

    private final DriverService driverService;


    @Autowired
    public DriverController(final DriverService driverService)
    {
        this.driverService = driverService;
    }

    @GetMapping("/{driverId}")
    public DriverDTO getDriver(@Valid @PathVariable long driverId) throws EntityNotFoundException
    {
        return DriverMapper.makeDriverDTO(driverService.find(driverId));
    }
    
    @GetMapping("/{driverId}/view-car")
    public DriverDTO viewCarDriven(@Valid @PathVariable long driverId) throws EntityNotFoundException
    {
    	return DriverMapper.makeCarDTO(driverService.find(driverId));
    }
    
    @GetMapping("/{driverId}/select-car/{carId}") 
    public DriverDTO selectCarDrive(@Valid @PathVariable long driverId, @Valid @PathVariable long carId) throws EntityNotFoundException, CarAlreadyInUseException
    {
    	return DriverMapper.makeDriverDTO(driverService.selectCar(driverId, carId));
    }
    
    @PutMapping("/{driverId}/deselect-car")
    public void deselectCarDriven(@Valid @PathVariable long driverId) throws EntityNotFoundException
    {
    	driverService.deselectCar(driverId);
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public DriverDTO createDriver(@Valid @RequestBody DriverDTO driverDTO) throws ConstraintsViolationException
    {
        DriverDO driverDO = DriverMapper.makeDriverDO(driverDTO);
        return DriverMapper.makeDriverDTO(driverService.create(driverDO));
    }


    @DeleteMapping("/{driverId}")
    public void deleteDriver(@Valid @PathVariable long driverId) throws EntityNotFoundException
    {
        driverService.delete(driverId);
    }


    @PutMapping("/{driverId}")
    public void updateLocation(
        @Valid @PathVariable long driverId, @RequestParam double longitude, @RequestParam double latitude)
        throws ConstraintsViolationException, EntityNotFoundException
    {
        driverService.updateLocation(driverId, longitude, latitude);
    }


    @GetMapping
    public List<DriverDTO> findDrivers(@RequestParam OnlineStatus onlineStatus)
        throws ConstraintsViolationException, EntityNotFoundException
    {
        return DriverMapper.makeDriverDTOList(driverService.find(onlineStatus));
    }
    
    // Using filter pattern 
    // Get all drivers with an engine type and ratings greater than 3
    @GetMapping("/type")
    public List<DriverDTO> findRatedDrivers(@RequestParam EngineType engine)
    		throws ConstraintsViolationException, EntityNotFoundException
    		{
    	return DriverMapper.makeDriverDTOList2(driverService.findEngineDrivers(engine));
    		}
    
}
