package com.sols.domainvalue;

import com.fasterxml.jackson.annotation.JsonProperty;
import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class Manufacturer
{

	@Column(name = "name")
    private final String name;


    protected Manufacturer()
    {
        this.name = null;
    }

    public Manufacturer(final String name)
    {
        this.name = name;
    }


    @JsonProperty
    public String getName()
    {
        return this.name;
    }

}
