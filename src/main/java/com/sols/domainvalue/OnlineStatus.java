package com.sols.domainvalue;

public enum OnlineStatus
{
    ONLINE, OFFLINE
}
