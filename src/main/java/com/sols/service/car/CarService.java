package com.sols.service.car;

import com.sols.domainobject.CarDO;
import com.sols.exception.ConstraintsViolationException;
import com.sols.exception.EntityNotFoundException;

import java.util.List;

public interface CarService
{

    CarDO find(Long carId) throws EntityNotFoundException;

    CarDO create(CarDO carDO) throws ConstraintsViolationException;
    
    void updateCar(CarDO carDO) throws ConstraintsViolationException;

    void delete(Long carId) throws EntityNotFoundException;
    
    List<CarDO> findAllCars();

}
