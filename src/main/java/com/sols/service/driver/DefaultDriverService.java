package com.sols.service.driver;

import com.sols.dataaccessobject.DriverRepository;
import com.sols.domainobject.CarDO;
import com.sols.domainobject.DriverDO;
import com.sols.domainvalue.EngineType;
import com.sols.domainvalue.GeoCoordinate;
import com.sols.domainvalue.OnlineStatus;
import com.sols.exception.CarAlreadyInUseException;
import com.sols.exception.ConstraintsViolationException;
import com.sols.exception.EntityNotFoundException;
import com.sols.service.car.CarService;

import java.util.List;

import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service to encapsulate the link between DAO and controller and to have business logic for some driver specific things.
 * <p/>
 */
@Service
public class DefaultDriverService implements DriverService
{

    private static org.slf4j.Logger LOG = LoggerFactory.getLogger(DefaultDriverService.class);

    @Autowired
    private CarService carService;
    
    private final DriverRepository driverRepository;

    public DefaultDriverService(final DriverRepository driverRepository)
    {
        this.driverRepository = driverRepository;
    }


    /**
     * Selects a driver by id.
     *
     * @param driverId
     * @return found driver
     * @throws EntityNotFoundException if no driver with the given id was found.
     */
    @Override
    public DriverDO find(Long driverId) throws EntityNotFoundException
    {
        return findDriverChecked(driverId);
    }
    
    /**
     * Select a car for a driver by id.
     *
     * @param driverId
     * @return found driver
     * @throws EntityNotFoundException if no driver with the given id was found.
     */
    @Override
    public DriverDO selectCar(Long driverId, Long carId) throws EntityNotFoundException, CarAlreadyInUseException
    {
    	DriverDO driverDO = findOnlineDriverChecked(driverId);
    	CarDO carDO = carService.find(carId);
    	if(carDO.getDriverDO() != null) {
    		throw new CarAlreadyInUseException("Car in use exception.");
    	}
    	carDO.setDriverDO(driverDO);
    	driverDO.setCarDo(carDO);
    	driverDO = driverRepository.save(driverDO);
    	return driverDO;
    }
    
    /**
     * Deselect a car.
     *
     * @param driverId
     * @return found driver
     * @throws EntityNotFoundException if no driver with the given id was found.
     */
    @Transactional
    public void deselectCar(Long driverId) throws EntityNotFoundException {
    	DriverDO driverDO = findDriverChecked(driverId);
    	driverDO.getCarDo().setDriverDO(null);
    	driverRepository.save(driverDO);
    }

    /**
     * Creates a new driver.
     *
     * @param driverDO
     * @return
     * @throws ConstraintsViolationException if a driver already exists with the given username, ... .
     */
    @Override
    public DriverDO create(DriverDO driverDO) throws ConstraintsViolationException
    {
        DriverDO driver;
        try
        {
            driver = driverRepository.save(driverDO);
        }
        catch (DataIntegrityViolationException e)
        {
            LOG.warn("Some constraints are thrown due to driver creation", e);
            throw new ConstraintsViolationException(e.getMessage());
        }
        return driver;
    }


    /**
     * Deletes an existing driver by id.
     *
     * @param driverId
     * @throws EntityNotFoundException if no driver with the given id was found.
     */
    @Override
    @Transactional
    public void delete(Long driverId) throws EntityNotFoundException
    {
        DriverDO driverDO = findDriverChecked(driverId);
        driverDO.setDeleted(true);
    }


    /**
     * Update the location for a driver.
     *
     * @param driverId
     * @param longitude
     * @param latitude
     * @throws EntityNotFoundException
     */
    @Override
    @Transactional
    public void updateLocation(long driverId, double longitude, double latitude) throws EntityNotFoundException
    {
        DriverDO driverDO = findDriverChecked(driverId);
        driverDO.setCoordinate(new GeoCoordinate(latitude, longitude));
    }


    /**
     * Find all drivers by online state.
     *
     * @param onlineStatus
     */
    @Override
    public List<DriverDO> find(OnlineStatus onlineStatus)
    {
        return driverRepository.findByOnlineStatus(onlineStatus);
    }
    
    /**
     * Find drivers with GAS Engines
     *
     * @param null
     */
    @Override
    public List<DriverDO> findEngineDrivers(EngineType engineType)
    {
    	return driverRepository.findByCarDoEngineType(engineType);
    }


    private DriverDO findDriverChecked(Long driverId) throws EntityNotFoundException
    {
        DriverDO driverDO = driverRepository.findOne(driverId);
        if (driverDO == null)
        {
            throw new EntityNotFoundException("Could not find entity with id: " + driverId);
        }
        return driverDO;
    }
    
    private DriverDO findOnlineDriverChecked(Long driverId) throws EntityNotFoundException
    {
    	DriverDO driverDO = driverRepository.findByIdAndOnlineStatus(driverId, OnlineStatus.ONLINE);
    	if (driverDO == null || driverDO.getCarDo() != null)
    	{
    		throw new EntityNotFoundException("Car already selected or could not find online entity with id: " + driverId);
    	}
    	return driverDO;
    }
    
}
