package com.sols.service.driver;

import com.sols.domainobject.DriverDO;
import com.sols.domainvalue.EngineType;
import com.sols.domainvalue.OnlineStatus;
import com.sols.exception.CarAlreadyInUseException;
import com.sols.exception.ConstraintsViolationException;
import com.sols.exception.EntityNotFoundException;

import java.util.List;

public interface DriverService
{

    DriverDO find(Long driverId) throws EntityNotFoundException;
    
    DriverDO selectCar(Long driverId, Long carId) throws EntityNotFoundException, CarAlreadyInUseException;
    
    void deselectCar(Long driverId) throws EntityNotFoundException;

    DriverDO create(DriverDO driverDO) throws ConstraintsViolationException;

    void delete(Long driverId) throws EntityNotFoundException;

    void updateLocation(long driverId, double longitude, double latitude) throws EntityNotFoundException;

    List<DriverDO> find(OnlineStatus onlineStatus);
    
    List<DriverDO> findEngineDrivers(EngineType engineType);

}
