package com.sols.domainobject;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.sols.domainvalue.EngineType;
import com.sols.domainvalue.Manufacturer;

import java.time.ZonedDateTime;

import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.Digits;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;
import org.hibernate.validator.constraints.Range;
import org.springframework.format.annotation.DateTimeFormat;

@Entity
@Table(
    name = "car",
    uniqueConstraints = @UniqueConstraint(name = "lp_license_plate", columnNames = {"license_plate"})
)
@Cacheable
@SQLDelete(sql="update car set deleted=true where id=?")
@Where(clause="deleted = false")
public class CarDO
{

    @Id
    @GeneratedValue
    private Long id;

    @Column(nullable = false)
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    private ZonedDateTime dateCreated = ZonedDateTime.now();

    @Column(nullable = false)
    @NotNull(message = "License plate can not be null!")
    private String license_plate;

    @Column(nullable = false)
    @NotNull(message="Please specify seat count.")
	@Digits(integer=11, fraction=0, message="Seat count must be integers only")
    private Integer seat_count;
    
    @Column(nullable = false)
    @Range(min=1, max=5, message="Integer must be integers from 1 - 5 only")
    private Integer rating;

    @Column(nullable = false)
    @NotNull(message = "Convertible must not be null")
    private Boolean convertible = false;
    
    @Column(nullable = false)
    @JsonIgnore
    private Boolean deleted = false;

    @Embedded
    private Manufacturer manufacturer;

    @Enumerated(EnumType.STRING)
    @Column(nullable = false)
    private EngineType engineType;
    
    @OneToOne
    @JoinColumn(name="driver_id", referencedColumnName="id", nullable=true)
    @JsonBackReference
    private DriverDO driverDO;


    private CarDO()
    {
    }


    public CarDO(String license_plate, Integer seat_count, Integer rating, Boolean convertible, Manufacturer manufacturer, EngineType engineType)
    {
        this.license_plate = license_plate;
        this.seat_count = seat_count;
        this.rating = rating;
        this.convertible = convertible;
        this.deleted = false;
        this.manufacturer = manufacturer;
        this.engineType = engineType;
    }


	public Long getId() {
		return id;
	}


	public void setId(Long id) {
		this.id = id;
	}

	public String getLicense_plate() {
		return license_plate;
	}

	public Integer getSeat_count() {
		return seat_count;
	}

	public Integer getRating() {
		return rating;
	}

	public void setRating(Integer rating) {
		this.rating = rating;
	}


	public Boolean getConvertible() {
		return convertible;
	}

	public Boolean getDeleted() {
		return deleted;
	}

	public void setDeleted(Boolean deleted) {
		this.deleted = deleted;
	}

	public Manufacturer getManufacturer() {
		return manufacturer;
	}

	public void setManufacturer(Manufacturer manufacturer) {
		this.manufacturer = manufacturer;
	}

	public EngineType getEngineType() {
		return engineType;
	}

	public void setEngineType(EngineType engineType) {
		this.engineType = engineType;
	}


	public DriverDO getDriverDO() {
		return driverDO;
	}


	public void setDriverDO(DriverDO driverDO) {
		this.driverDO = driverDO;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((convertible == null) ? 0 : convertible.hashCode());
		result = prime * result
				+ ((engineType == null) ? 0 : engineType.hashCode());
		result = prime * result
				+ ((license_plate == null) ? 0 : license_plate.hashCode());
		result = prime * result + ((rating == null) ? 0 : rating.hashCode());
		result = prime * result
				+ ((seat_count == null) ? 0 : seat_count.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CarDO other = (CarDO) obj;
		if (convertible == null) {
			if (other.convertible != null)
				return false;
		} else if (!convertible.equals(other.convertible))
			return false;
		if (engineType != other.engineType)
			return false;
		if (license_plate == null) {
			if (other.license_plate != null)
				return false;
		} else if (!license_plate.equals(other.license_plate))
			return false;
		if (rating == null) {
			if (other.rating != null)
				return false;
		} else if (!rating.equals(other.rating))
			return false;
		if (seat_count == null) {
			if (other.seat_count != null)
				return false;
		} else if (!seat_count.equals(other.seat_count))
			return false;
		return true;
	}

}
