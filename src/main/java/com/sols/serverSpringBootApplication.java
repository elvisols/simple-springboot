package com.sols;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;

import com.sols.util.LoggingInterceptor;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@EnableSwagger2
@SpringBootApplication
public class serverSpringBootApplication extends WebMvcConfigurerAdapter implements CommandLineRunner 
{

	private Logger logger = LoggerFactory.getLogger(this.getClass());
	
    public static void main(String[] args)
    {
        SpringApplication.run(serverSpringBootApplication.class, args);
    }


    @Override
    public void addInterceptors(InterceptorRegistry registry)
    {
        registry.addInterceptor(new LoggingInterceptor()).addPathPatterns("/**");
    }


    @Bean
    public Docket docket()
    {
        return new Docket(DocumentationType.SWAGGER_2)
            .select()
            .apis(RequestHandlerSelectors.basePackage(getClass().getPackage().getName()))
            .paths(PathSelectors.any())
            .build()
            .apiInfo(generateApiInfo());
    }


    private ApiInfo generateApiInfo()
    {
        return new ApiInfo("Spring Boot Test Service", "Service testing...", "Version 1.0 - mw",
            "urn:tos", "info@sols.com", "Apache 2.0", "http://www.apache.org/licenses/LICENSE-2.0");
    }


	@Override
	public void run(String... arg0) throws Exception {
		// This is a workaround to creating the authentication schema and populating with 3 demo client
		logger.info("Setting up OAuth2 schema ...");
		/*****        Comment Out the codes below BEFORE running JUNIT Test.    ******/
		Connection conn = DriverManager.getConnection("jdbc:h2:mem:testdb", "sa", "");
        Statement st = conn.createStatement();
        st.execute("create table oauth_client_details ("+
		  "client_id VARCHAR(255) PRIMARY KEY,"+
		  "resource_ids VARCHAR(255),"+
		  "client_secret VARCHAR(255),"+
		  "scope VARCHAR(255),"+
		  "authorized_grant_types VARCHAR(255),"+
		  "web_server_redirect_uri VARCHAR(255),"+
		  "authorities VARCHAR(255),"+
		  "access_token_validity INTEGER,"+
		  "refresh_token_validity INTEGER,"+
		  "additional_information VARCHAR(4096),"+
		  "autoapprove VARCHAR(255)"+
		")");
        st.execute("create table oauth_client_token ("+
		  "token_id VARCHAR(255),"+
		  "token LONGVARBINARY,"+
		  "authentication_id VARCHAR(255) PRIMARY KEY,"+
		  "user_name VARCHAR(255),"+
		  "client_id VARCHAR(255)"+
		")");
        st.execute("create table oauth_access_token ("+
		  "token_id VARCHAR(255),"+
		  "token LONGVARBINARY,"+
		  "authentication_id VARCHAR(255) PRIMARY KEY,"+
		  "user_name VARCHAR(255),"+
		  "client_id VARCHAR(255),"+
		  "authentication LONGVARBINARY,"+
		  "refresh_token VARCHAR(255)"+
		")");
        st.execute("create table oauth_refresh_token ("+
		  "token_id VARCHAR(255),"+
		  "token LONGVARBINARY,"+
		  "authentication LONGVARBINARY"+
		")");
        st.execute("create table oauth_code ("+
		  "code VARCHAR(255), "+
		  "authentication LONGVARBINARY"+
		")");
        st.execute("create table oauth_approvals ("+
        	"userId VARCHAR(255),"+
			"clientId VARCHAR(255),"+
			"scope VARCHAR(255),"+
			"status VARCHAR(10),"+
			"expiresAt TIMESTAMP,"+
			"lastModifiedAt TIMESTAMP"+
		")");
        st.execute("create table ClientDetails ("+
		  "appId VARCHAR(255) PRIMARY KEY,"+
		  "resourceIds VARCHAR(255),"+
		  "appSecret VARCHAR(255),"+
		  "scope VARCHAR(255),"+
		  "grantTypes VARCHAR(255),"+
		  "redirectUrl VARCHAR(255),"+
		  "authorities VARCHAR(255),"+
		  "access_token_validity INTEGER,"+
		  "refresh_token_validity INTEGER,"+
		  "additionalInformation VARCHAR(4096),"+
		  "autoApproveScopes VARCHAR(255)"+
		")");
        st.execute("INSERT INTO oauth_client_details"+
		"(client_id, client_secret, scope, authorized_grant_types,"+
		"web_server_redirect_uri, authorities, access_token_validity,"+
		"refresh_token_validity, additional_information, autoapprove)"+
		"VALUES"+
		"('fooClientIdPassword', 'secret', 'foo,read,write',"+
		"'password,authorization_code,refresh_token', null, null, 36000, 36000, null, true)");
        st.execute("INSERT INTO oauth_client_details"+
		"(client_id, client_secret, scope, authorized_grant_types,"+
		"web_server_redirect_uri, authorities, access_token_validity,"+
		"refresh_token_validity, additional_information, autoapprove)"+
		"VALUES"+
		"('sampleClientId', 'secret', 'read,write,foo,bar',"+
		"'implicit', null, null, 36000, 36000, null, false)");
        st.execute("INSERT INTO oauth_client_details"+
		"(client_id, client_secret, scope, authorized_grant_types,"+
		"web_server_redirect_uri, authorities, access_token_validity,"+
		"refresh_token_validity, additional_information, autoapprove)"+
		"VALUES"+
		"('barClientIdPassword', 'secret', 'bar,read,write',"+
		"'password,authorization_code,refresh_token', null, null, 36000, 36000, null, true)");

        logger.info("*************************  Service Application Started   ***********************");
	
	}

}
