package com.sols.dataaccessobject;

import com.sols.domainobject.DriverDO;
import com.sols.domainvalue.EngineType;
import com.sols.domainvalue.OnlineStatus;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

/**
 * Database Access Object for driver table.
 * <p/>
 */
public interface DriverRepository extends CrudRepository<DriverDO, Long>
{

    List<DriverDO> findByOnlineStatus(OnlineStatus onlineStatus);
    
    List<DriverDO> findByCarDoEngineType(EngineType engineType);
    
    DriverDO findByIdAndOnlineStatus(Long id, OnlineStatus onlineStatus);

}
