package com.sols.datatransferobject;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.sols.domainobject.DriverDO;

import javax.validation.constraints.Digits;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Range;

@JsonInclude(JsonInclude.Include.NON_NULL)
//@Document(collection="todos")
public class CarDTO
{
	
	@JsonIgnore
	private Long id;

    @NotNull(message = "License plate can not be null!")
    private String license_plate;

    @NotNull(message="Please specify seat count.")
	@Digits(integer=11, fraction=0, message="Seat count must be integers only")
    private Integer seat_count;
    
    @Range(min=1, max=5, message="Integer must be integers from 1 - 5 only")
    private Integer rating;

    @NotNull(message = "Convertible must not be null")
    private Boolean convertible = false;
    
    private String manufacturer;
    
    private DriverDO driverDO;

    @NotNull(message = "Engine type can not be null")
//    @Enumerated(EnumType.STRING)
    private String engineType;

    private CarDTO()
    {
    }

    private CarDTO(Long id, String license_plate, Integer seat_count, Integer rating, Boolean convertible, String manufacturer, String engineType, DriverDO driverDO)
    {
    	this.id = id;
    	this.license_plate = license_plate;
        this.seat_count = seat_count;
        this.rating = rating;
        this.convertible = convertible;
        this.manufacturer = manufacturer;
        this.engineType = engineType;
        this.driverDO = driverDO;
    }


    public static CarDTOBuilder newBuilder()
    {
        return new CarDTOBuilder();
    }


    @JsonProperty
	public Long getId() {
		return id;
	}

	public String getLicense_plate() {
		return license_plate;
	}

	public Integer getSeat_count() {
		return seat_count;
	}

	public Integer getRating() {
		return rating;
	}

	public Boolean getConvertible() {
		return convertible;
	}

	public String getManufacturer() {
		return manufacturer;
	}

	public String getEngineType() {
		return engineType;
	}
	
	public DriverDO getDriverDO() {
		return driverDO;
	}

    public static class CarDTOBuilder
    {
        
        private Long id;
        private String license_plate;
        private Integer seat_count;
        private Integer rating;
        private Boolean convertible = false;
        private String manufacturer;
        private String engineType;
        private DriverDO driverDO;

        public CarDTOBuilder setId(Long id)
        {
            this.id = id;
            return this;
        }


        public CarDTOBuilder setLicense_plate(String lp)
        {
            this.license_plate = lp;
            return this;
        }

        public CarDTOBuilder setSeat_count(Integer sc)
        {
            this.seat_count = sc;
            return this;
        }
        
        public CarDTOBuilder setRating(Integer r)
        {
        	this.rating = r;
        	return this;
        }
        
        public CarDTOBuilder setConvertible(Boolean c)
        {
        	this.convertible = c;
        	return this;
        }
        
        public CarDTOBuilder setEngineType(String et)
        {
        	this.engineType = et;
        	return this;
        }


        public CarDTOBuilder setManufacturer(String m)
        {
            this.manufacturer = m;
            return this;
        }
        
        public CarDTOBuilder setDriverDO(DriverDO d)
        {
        	this.driverDO = d;
        	return this;
        }

        public CarDTO createCarDTO()
        {
            return new CarDTO(id, license_plate, seat_count, rating, convertible, manufacturer, engineType, driverDO);
        }

    }
}
