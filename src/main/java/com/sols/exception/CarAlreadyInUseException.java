package com.sols.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.BAD_REQUEST, reason = "Oops! car already in use. Please try again later.")
public class CarAlreadyInUseException extends Exception
{
    static final long serialVersionUID = -2387516913334229943L;


    public CarAlreadyInUseException(String message)
    {
        super(message);
    }

}
