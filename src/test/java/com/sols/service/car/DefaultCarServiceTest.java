package com.sols.service.car;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;

import static org.hamcrest.CoreMatchers.*;
import static org.hamcrest.collection.IsIterableContainingInAnyOrder.containsInAnyOrder;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import com.sols.serverSpringBootApplication;
import com.sols.domainobject.CarDO;
import com.sols.domainvalue.EngineType;
import com.sols.domainvalue.Manufacturer;
import com.sols.exception.ConstraintsViolationException;
import com.sols.exception.EntityNotFoundException;
import com.sols.service.car.DefaultCarService;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = serverSpringBootApplication.class)

public class DefaultCarServiceTest {

	private Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	DefaultCarService carService;
	
	@Test
	public void findTest() throws EntityNotFoundException {
		CarDO carDO = carService.find(2L);
		logger.info("findTest {}", carDO.getManufacturer().getName());
		assertEquals(EngineType.GAS, carDO.getEngineType());
		assertEquals("XYZ123", carDO.getLicense_plate());
		assertEquals("Ford", carDO.getManufacturer().getName());
	}
	
	@Test
	@DirtiesContext
	@Transactional
	public void createTest() throws ConstraintsViolationException {
		// get a course
		CarDO carDO = carService.create(new CarDO("YRE981", 12, 4, true, new Manufacturer("Mitsubishi"), EngineType.ELECTRIC));
		logger.info("Manufacturer name", carDO.getManufacturer().getName());
		assertEquals("YRE981", carDO.getLicense_plate());
		assertEquals("Mitsubishi", carDO.getManufacturer().getName());
	}
	
	@Test
	@DirtiesContext
	@Transactional
	public void updateCarTest() throws ConstraintsViolationException, EntityNotFoundException {
		// get a course
		CarDO carDO = carService.find(3L);
		assertFalse(carDO.getDeleted());

		// update details
		carDO.setDeleted(true);
		carService.updateCar(carDO);

		// check the value
		CarDO carDO1 = carService.find(3L);
		assertTrue(carDO1.getDeleted());
	}
	
	@Test
	@DirtiesContext
	@Transactional
	public void deleteTest() throws EntityNotFoundException {
		carService.delete(3L);
		assertNull(carService.findCarChecked(3L));
	}
	
	@Test
	public void findAllCarsTest() {
		List<CarDO> allCars = carService.findAllCars();
//		logger.info("All cars {}", allCars);
		CarDO carDO1 = new CarDO("ABC123", 5, 5, false, new Manufacturer("Mercedes Benz"), EngineType.GAS);
		CarDO carDO2 = new CarDO("XYZ123", 4, 4, true, new Manufacturer("Ford"), EngineType.GAS);
		CarDO carDO3 = new CarDO("KLM123", 6, 3, false, new Manufacturer("Toyota"), EngineType.ELECTRIC);
		CarDO carDO4 = new CarDO("QWE123", 7, 4, false, new Manufacturer("Hyudai"), EngineType.FUEL);
		assertThat(allCars, containsInAnyOrder(
                carDO1,
                carDO2,
                carDO3,
                carDO4
        ));
	}
	
}
