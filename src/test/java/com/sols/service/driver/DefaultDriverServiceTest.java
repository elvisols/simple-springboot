package com.sols.service.driver;

import static org.junit.Assert.*;

import java.util.List;

import javax.transaction.Transactional;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.collection.IsIterableContainingInAnyOrder.containsInAnyOrder;

import com.sols.serverSpringBootApplication;
import com.sols.domainobject.CarDO;
import com.sols.domainobject.DriverDO;
import com.sols.domainvalue.EngineType;
import com.sols.domainvalue.Manufacturer;
import com.sols.exception.CarAlreadyInUseException;
import com.sols.exception.EntityNotFoundException;
import com.sols.service.driver.DefaultDriverService;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = serverSpringBootApplication.class)
public class DefaultDriverServiceTest {
	
	private Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	DefaultDriverService driverService;
	
	@Rule
    public ExpectedException thrown = ExpectedException.none();
	
	@Test
	public void selectCarExceptionTest() throws EntityNotFoundException, CarAlreadyInUseException {
		
        thrown.expect(CarAlreadyInUseException.class);

        thrown.expectMessage(containsString("Car in use"));
        
		DriverDO driverDO = driverService.selectCar(8L, 1L);
		logger.info("Car already in use exception {}", driverDO);
	}
	
	@Test
	@Transactional
	public void selectCarSuccessTest() throws EntityNotFoundException, CarAlreadyInUseException {
		
		DriverDO driverDO = driverService.selectCar(8L, 3L);
		logger.info("Car assigned {}", driverDO);
		assertEquals("Toyota", driverDO.getCarDo().getManufacturer().getName());
	}

	// Get drivers records with car engine GAS
	@Test
	public void engineDriversTest() {
		
		List<DriverDO> eDrivers = driverService.findEngineDrivers(EngineType.GAS);
		logger.info("Drivers engine type {}", eDrivers);
		DriverDO driverDO4 = new DriverDO("driver04", "driver04pw");
		DriverDO driverDO6 = new DriverDO("driver06", "driver06pw");
		assertThat(eDrivers, containsInAnyOrder(
                driverDO4,
                driverDO6
        ));
	}
	
}
